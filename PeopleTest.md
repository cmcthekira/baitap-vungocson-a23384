
public class PeopleTest {
	public static void main(String[] args) {
		Employee newbie = new Employee("Newbie", "10/2/1989", 2000);
		Manager boss = new Manager("Boss", "23/2/1979", 4000000);
		Manager bigBoss = new Manager("Big Boss", "3/12/1969", 10000000);
		
		newbie.hienthi();
		boss.hienthi();
		bigBoss.hienthi();			
	}	
}
